# Kristhyan de Matos Maia
# Unifesspa - Universidade Federal do Sul e Sudeste do Pará
# IGE - Instituto de Geociência e Engenharia
# # Faceel - Faculdade de Computação e Engenharia Elétrica
# Engenharia da Computação 2017

# 1º Lista de Exercícios de Avaliação e Desempenho de sistemas
# Teoria das filas


# Parametros no enunciado da questão
navios = 20
duracao_1 = [5, 5, 3, 3, 6, 7, 6, 8, 2, 5, 8, 8, 8, 3, 4, 3, 3, 4, 5, 5]
intervalo_1 = [10, 2, 13, 7, 2, 8, 8, 8, 10, 9, 1, 14, 14, 1, 10, 9, 9, 9, 8, 14]

duracao_2 = [5, 3, 5, 3, 2, 5, 8, 8, 3, 4, 5, 5, 6, 7, 6, 8, 8, 3, 4, 3]
intervalo_2 = [9, 9, 8, 14, 10, 2, 13, 8, 8, 10, 9, 1, 14, 14, 1, 10, 9, 7, 2, 8]


def a(intervalo, navios):
    return sum(intervalo) / navios


def b(duracao, navios):
    return sum(duracao) / navios


def soma_lista_espera(intervalo, duracao, navios):
    lista_espera = [0]
    soma_de_espera = 0
    espera_anterior = 0

    for index in range(1, navios):
        tempo_de_sobra_entre_eles = intervalo[index] - (
            espera_anterior + duracao[index - 1]
        )
        # Descomente para ver cálculo em cada posição
        # print(
        #     "[",
        #     index + 1,
        #     "]",
        #     " R = ",
        #     intervalo[index],
        #     " - (",
        #     espera_anterior,
        #     " + ",
        #     duracao[index - 1],
        #     ") = ",
        #     abs(tempo_de_sobra_entre_eles),
        # )

        if tempo_de_sobra_entre_eles < 0:
            espera_anterior = abs(tempo_de_sobra_entre_eles)
            soma_de_espera += espera_anterior
            lista_espera.append(espera_anterior)
        else:
            lista_espera.append(0)
            espera_anterior = 0

    return soma_de_espera


def c(intervalo, duracao, navios):
    soma_espera = soma_lista_espera(intervalo, duracao, navios)

    return round((soma_espera / sum(intervalo)), 2)

    # Descomente para ver a lista de espera dos navios
    # print(espera)


def d(intervalo, duracao, navios):
    soma_espera = soma_lista_espera(intervalo, duracao, navios)

    return round((soma_espera / navios), 2)


# 1) Considere um sistema em que navios chegam algum produto.
print("1º Questão")
# Pede-se:

# O intervalo médio entre chegadas
print("A) ", a(intervalo_1, navios))

# A duração média da carga
print("B) ", b(duracao_1, navios))

# Calcule o tamanho médio da fila
print("C) ", c(intervalo_1, duracao_1, navios))

# Calcule o tempo médio de espera na fila
print("D) ", d(intervalo_1, duracao_1, navios))

print()
# 2) Escreva os valores acima, referente aos intervalos entre chegadas,
# em pequenos pedaços de papel, dobrando-os em seguida como se preparasse os mesmos para sorteio.
# Misture os pedaços de papel e, em seguida, vá abrindo de um por um e anotando os valores encontrados, faça isso até o último.
# Dessa forma você deve ter obtido uma nova sequência de valores para os intervalos entre chegadas.
# Repita o processo para as durações do atendimento.
# E logo em seguida refaça o exercício anterior.
print("2º Questão")

# O intervalo médio entre chegadas
print("A) ", a(intervalo_2, navios))

# A duração média da carga
print("B) ", b(duracao_2, navios))

# Calcule o tamanho médio da fila
print("C) ", c(intervalo_2, duracao_2, navios))

# Calcule o tempo médio de espera na fila
print("D) ", d(intervalo_2, duracao_2, navios))

print()
# 3) Compare os resultados dos exercícios 1 e 2.
# Explique o que aconteceu, e porquê:
print("3º Questão")
print(
    "Podemos observar que dependendo da ordem",
    "de chega dos navios, com seus devidos atributos,",
    "o tamanho médio da fila e o tempo médio de espera varia,",
    "entrentanto, o intervalo médio de chegada e o ",
    "tempo médio de atendimento (carga) permanece o mesmo.",
    "Com isso podemos concluir que de acordo com dada organização",
    "se pode ter uma serviço rápido ou lento, independente do",
    "intervalo médio de chegada e do tempo médio de atendimento.",
)
